package br.com.itau.projeto.dados;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Exercicios {

   public static void main(String[] args) {

      /**
       * Exercício 1
       */
      Random random1 = new Random();
      int numeroSorteado1 = random1.nextInt(6);
      System.out.println("Numero sorteado do exercicio 1: " + numeroSorteado1 + "\n");


      /**
       * Exercicio 2
       */
      Random random2 = new Random();
      int contadorNumeros = 0;

      for (int i =0; i <= 3; i++){
         int numerosSorteados2 = random2.nextInt(6);
         System.out.println(i + " - Número sorteado exercicio 2: " + numerosSorteados2);
         contadorNumeros = contadorNumeros+numerosSorteados2;
      }

      System.out.println("--- Total dos números sorteados: "+contadorNumeros + "\n");


      /**
       * Exercicio 3
       */
      Random random3 = new Random();
      int contadorNumeroEx3grupo = 0;
      int quantidadeDeGrupos = 3;

      while (quantidadeDeGrupos > 0){
         List grupo = new ArrayList();
         for (int x = 0; x < 3; x++) {
            int numeroSorteado3 = random3.nextInt(6);
            grupo.add(numeroSorteado3);
            contadorNumeroEx3grupo = contadorNumeroEx3grupo + numeroSorteado3;
         }
         quantidadeDeGrupos --;

         grupo.add(contadorNumeroEx3grupo);
         System.out.println("--- Numeros sorteados exercicio 3: "  + grupo);
         contadorNumeroEx3grupo = 0;
      }




   }


}
